# Project Setup Homework

**For the actual homework assignment, go to [Project Setup Homework.md](./ProjectSetupHomework.md).**

This assignment has a document with instructions for the assignment.

**You do not need to fork and clone this repository. You will complete the assignment entirely in GitLab and Visual Studio Code.**

## Requires

* Git
* Visual Studio Code

Copyright © 2023 Karl R. Wurst.

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
