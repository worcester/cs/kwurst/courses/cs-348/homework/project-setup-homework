# Project Setup Homework

## Objectives

* Be able to setup a project on GitLab with an issues board
* Be able to a follow a simple workflow for issues
* Be able to add an License to a project
* Configure VSCode for a project
* Create a VSCode Dev Container
* Build simple bash scripts for a project

## *Base Assignment*

**Everyone must complete this portion of the assignment in a
*Meets Specification* fashion.**

Your project can be anything you want, in any language(s) you want. If you
don't have an idea for a project, you can use a project you created for
another course. It should have at least two code files (.class files if it
is a Java project.)

### Create a GitLab project for your project

1. In your GitLab student subgroup under the CS-348 01 02 Fall 2023 group,
create a project. Your subgroup can be accessed here:
[https://gitlab.com/worcester/cs/cs-348-01-02-fall-2023/students/](https://gitlab.com/worcester/cs/cs-348-01-02-fall-2023/students/)
2. Set up an Issue Board for your project with columns for
    1. Product Backlog
    2. Sprint Backlog
    3. In Process
    4. Needs Review
    5. Done
3. Add your LICENSE file.
4. Create at least 4 issues on your board for the items in the next section.
Use your board to move them through the appropriate columns as you work on
them.

### Clone your project and configure the project in VSCode

1. Clone your project to your computer.
2. Set any VSCode Workspace Settings you need for your project. (At the
very least set `Files: Insert Final Newline` and `Files: Trim Final Newlines`)
3. Add VSCode extensions to the workspace for:
    * Creating a .gitignore file
    * Creating a .gitattributes file
    * Markdown Lint
4. Create required files based on your project. Make sure you include whatever
language(s) you are coding in, markdown, common files, Linux, Windows, Mac OS:
    1. `.gitignore`
    2. `.gitattributes`
5. Edit your README.md file so that someone coming to your project for the
first time knows about the project. Include at least:
    1. Name of the project
    2. Description of the project and what it does/is used for.
    3. Information about language(s) used.
    4. Author information

## *Intermediate Add-On*

**Complete this portion in a *Meets Specification* fashion to apply towards
base course grades of C or higher.**

* See the the Course Grade Determination table in the syllabus to see how many
Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received
an Acceptable grade on.
* You may not need to submit one for this assignment.

### Create a VSCode Dev Container for your project

1. Create a VSCode Dev Container for your project that installs the appropriate language(s) for your project.
2. Make sure all of the recommended extensions for your project are installed in the Dev Container.
3. Make sure all your Workspace settings are installed in the Dev Container.

## *Advanced Add-On*

**Complete this portion in a *Meets Specification* fashion to apply towards
base course grades of B or higher.**

* See the the Course Grade Determination table in the syllabus to see how many
Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received
an Acceptable grade on.
* You may not need to submit one for this assignment.

### Create Command Scripts

1. Create a build.sh and/or run.sh script for your project. They should compile your project (if your language(s) are compiled) and/or run your project. 
2. Create a lint.sh script for your project. It should lint, at least, Markdown.

#### Deliverables

* Complete the portions of the assignment above in your project.
* Add and commit your changes.
* Push your changes.

&copy; 2023 Karl R. Wurst and Stoney Jackson.

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.

